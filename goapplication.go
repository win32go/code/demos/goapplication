/* goapplication.go */

package main

import (
	"log"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	win "gitlab.com/win32go/win32"
)

var desktop *win.GoDeskTopWindow
var mainwindow *win.GoWindowView

func main() {
	var screenMetrics string

	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app := win.GoApplication(appName)

	desktop = app.GoDeskTop()
	
	screenMetrics = "Screen Height ...... " + strconv.Itoa(desktop.Height()) + " pixels\r\n"
	screenMetrics += "Screen Width ....... " + strconv.Itoa(desktop.Width()) + " pixels\r\n"

	screenMetrics += "Screen ClientHeight ...... " + strconv.Itoa(desktop.ClientHeight()) + " pixels\r\n"
	screenMetrics += "Screen ClientWidth ....... " + strconv.Itoa(desktop.ClientWidth()) + " pixels\r\n"

	screenMetrics += "Screen Vertical Size ...... " + strconv.Itoa(desktop.VerticalSize()) + " mm\r\n"
	screenMetrics += "Screen Horizontal Size .... " + strconv.Itoa(desktop.HorizontalSize()) + " mm\r\n"

	screenMetrics += "Screen Vertical Res ...... " + strconv.Itoa(desktop.VerticalRes()) + " pixels/inch\r\n"
	screenMetrics += "Screen Horizontal Res .... " + strconv.Itoa(desktop.HorizontalRes()) + " pixels/inch\r\n"

	

	mainwindow = win.GoWindow(nil)
	mainwindow.SetTitle("GoApplication Demo")
	mainwindow.SetMargin(40)
	mainwindow.SetOnPaint(MainWindow_Paint)
	mainwindow.Show()

	mainwindow.TextOut(screenMetrics)
	
	ret := app.Run()
	fmt.Println("Window Closed return =", ret)
}

func MainWindow_Paint(p *win.GoPainter) {
	log.Println("MainWindow_Paint..............")
	blue := win.CreateRGBColor(0, 0, 255)
	red := win.CreateRGBColor(255, 0, 0)
	//r, g, b := blue.RGB()
	//log.Println("blue = r:", strconv.Itoa(int(r)), "g:", strconv.Itoa(int(g)), "b:", strconv.Itoa(int(b)) )
	//log.Println("blue = ", strconv.Itoa(int(blue.W32Color())) )
	log.Println("SetStockPenColor(blue):")
	p.SetStockPenColor(red)
	
	log.Println("DrawRect............")
	p.DrawRect(p.X() - 5, p.Y() - 5, p.Width() + 10, p.Height() + 10)
	//p.ResetPen()
	log.Println("CreatePen.............")
	pen := win.CreatePen(blue, 3, 1, "blue5")
	log.Println("pen:", pen)
	log.Println("SetPen(pen):")
	p.SetPen(pen)
	//log.Println("SetStockPenColor(red):")
	//p.SetStockPenColor(red)
	log.Println("DrawRect............")
	p.DrawRect(p.X()-10, p.Y()-10, p.Width()+20, p.Height()+20)

}